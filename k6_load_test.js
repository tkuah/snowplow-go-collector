import { check } from "k6";
import http from 'k6/http';
export default function() {
  let res = http.get('http://localhost:8080/i?stm=1585427766447&e=pv&url=http%3A%2F%2Flocalhost%3A3000%2Fh5bp%2Fhtml5-boilerplate%2F-%2Fproduct_analytics%2Ftest&page=H5bp%20%2F%20Html5%20Boilerplate%20%C2%B7%20GitLab&refr=http%3A%2F%2Flocalhost%3A3000%2Fh5bp%2Fhtml5-boilerplate%2F-%2Fproduct_analytics%2Ftest&tv=js-2.14.0&tna=sp&aid=8&p=app&tz=America%2FLos_Angeles&lang=en-US&cs=UTF-8&f_pdf=1&f_qt=0&f_realp=0&f_wma=0&f_dir=0&f_fla=0&f_java=0&f_gears=0&f_ag=0&res=3008x1692&cd=24&cookie=1&eid=a6921333-d118-4b97-a7de-f8a27926c089&dtm=1585427766429&vp=616x1478&ds=601x1478&vid=4&sid=b01e1c4a-9bd4-415b-9ea7-ca19a89b68ef&duid=bb934f79-0492-4093-902f-21224240381c');
  check(res, {
    "is status 200": r => r.status === 200
  });
}
